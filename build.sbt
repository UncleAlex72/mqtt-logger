import com.typesafe.sbt.packager.docker.Cmd
import sbt.Keys._
import sbt._
import sbtrelease.ReleasePlugin.autoImport.ReleaseTransformations._

lazy val `mqtt-logger` =
  project
    .in(file("."))
    .enablePlugins(
      DockerPlugin,
      AshScriptPlugin
    )
    .settings(
      scalaVersion := "2.13.2",
      organization := "uk.co.unclealex",
      scalacOptions += "-Ymacro-annotations",
      libraryDependencies ++= Seq(
        "org.typelevel" %% "cats-core" % "2.3.0",
        "com.github.pureconfig" %% "pureconfig" % "0.15.0",
        "com.typesafe.scala-logging" %% "scala-logging" % "3.9.3",
        "com.lightbend.akka" %% "akka-stream-alpakka-mqtt" % "3.0.0",
        "ch.qos.logback" % "logback-classic" % "1.2.3",
        "org.reactivemongo" %% "reactivemongo" % "0.20.13",
        "com.typesafe.play" %% "play-json" % "2.9.0",
        "org.reactivemongo" %% "reactivemongo-play-json-compat" % "0.20.13-play29",
        "com.typesafe.akka" %% "akka-stream-testkit" % "2.6.14" % Test,
        "org.scalatest" %% "scalatest" % "3.2.9" % Test,
        "de.flapdoodle.embed" % "de.flapdoodle.embed.mongo" % "3.0.0" % Test,
        "com.google.jimfs" % "jimfs" % "1.2" % Test
      ),
      // Docker
      dockerBaseImage := "adoptopenjdk/openjdk11-openj9:alpine",
      maintainer := "Alex Jones <alex.jones@unclealex.co.uk>",
      dockerRepository := Some("unclealex72"),
      packageName := "mqtt-logger",
      dockerUpdateLatest := true,
      javaOptions in Universal += "-Dpidfile.path=/dev/null",
      Global / onChangedBuildSource := ReloadOnSourceChanges,
      ivyLoggingLevel := UpdateLogging.Quiet,
      scalacOptions += "-Ymacro-annotations",
      dockerPackageMappings in Docker += (baseDirectory.value / "src" / "main" / "logrotate" / "mqtt.conf") -> "mqtt.logrotate",
      dockerCommands := {
        val (prefix, suffix) = dockerCommands.value.splitAt(16)
        prefix ++ Seq(
          Cmd(
            "Add",
            "mqtt.logrotate",
            "/etc/logrotate.d/mqtt"
          ),
          Cmd("Run", "chmod", "og-w", "/etc/logrotate.d/mqtt"),
          Cmd("Run", "apk", "add", "logrotate")
        ) ++ suffix
      },
      releaseProcess := Seq[ReleaseStep](
        releaseStepCommand("scalafmtCheckAll"),
        releaseStepCommand("scalafmtSbtCheck"),
        inquireVersions, // : ReleaseStep
        runTest, // : ReleaseStep
        setReleaseVersion, // : ReleaseStep
        commitReleaseVersion, // : ReleaseStep, performs the initial git checks
        tagRelease, // : ReleaseStep
        releaseStepCommand(
          "packageBin"
        ), // : ReleaseStep, build server docker image
        releaseStepCommand(
          "docker:publish"
        ), // : ReleaseStep, build server docker image
        setNextVersion, // : ReleaseStep
        commitNextVersion, // : ReleaseStep
        pushChanges // : ReleaseStep, also checks that an upstream branch is properly configured
      )
    )
