addSbtPlugin("com.typesafe.sbt" % "sbt-git" % "1.0.0")

addSbtPlugin("com.github.gseitz" % "sbt-release" % "1.0.13")

addSbtPlugin("com.typesafe.sbt" % "sbt-gzip" % "1.0.2")

addSbtPlugin("com.typesafe.sbt" % "sbt-digest" % "1.1.4")

addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.7.4")

addSbtPlugin("org.scalameta" % "sbt-scalafmt" % "2.4.2")

ivyLoggingLevel in ThisBuild := UpdateLogging.Quiet
