package mqttlogger

import akka.stream.scaladsl.Source
import reactivemongo.api.Cursor
import reactivemongo.api.bson._

class MongoDbSinkSpec extends MongoDbSpec("MongoDbSinkSpec") {

  "The MongoDbSink" should "create a collection where the topic and timestamp are non-unique indices" in {
    url =>
      val sink = Persistence.mongoDbSink(driver, None, url)
      for {
        collection <- Source.empty[BSONDocument].runWith(sink)
        indices <- collection.indexesManager.list()
      } yield {
        val columnsAndUniqueness: Seq[(String, Boolean)] = indices.map {
          index =>
            index.key.map(_._1).mkString -> index.unique
        }
        columnsAndUniqueness should contain theSameElementsAs Seq(
          Persistence.TIMESTAMP -> false,
          Persistence.TOPIC -> false,
          Persistence._ID -> false
        )
      }
  }

  it should "store all the documents that are sent to it" in { url =>
    val freddie = BSONDocument(
      Seq("_id" -> BSONObjectID.generate(), "name" -> BSONString("Freddie"))
    )
    val brian = BSONDocument(
      Seq("_id" -> BSONObjectID.generate(), "name" -> BSONString("Brian"))
    )
    val sink = Persistence.mongoDbSink(driver, None, url)
    for {
      collection <- Source(Seq(brian, freddie)).runWith(sink)
      elements <-
        collection
          .find(BSONDocument.empty, Option.empty[BSONDocument])
          .cursor[BSONDocument]()
          .collect[List](-1, Cursor.FailOnError[List[BSONDocument]]())
    } yield {
      elements should contain theSameElementsAs Seq(
        brian,
        freddie
      )
    }
  }
}
