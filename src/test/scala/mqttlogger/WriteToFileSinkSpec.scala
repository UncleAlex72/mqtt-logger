package mqttlogger

import akka.stream.scaladsl.Source

import java.nio.file.{FileSystem, Files, Path}
import java.time.{Clock, Instant, ZoneId}
import scala.jdk.CollectionConverters._

class WriteToFileSinkSpec extends JimFsSpec("writeToFileSyncSpec") {

  val clock: Clock = Clock.fixed(
    Instant.ofEpochMilli(1637517386714L),
    ZoneId.of("Europe/London")
  )
  def defaultPath(fs: FileSystem) =
    fs.getPath("/", "var", "log", "mqtt", "out.log")

  "The write to file sink" should
    "create a new file if it does not exist" in { fs =>
    val path = defaultPath(fs)
    Source
      .single[(Path, String)]((path, "Hello"))
      .runWith(Logging.writeToFileSink)
      .map { _ =>
        Files
          .readAllLines(path)
          .asScala should contain theSameElementsInOrderAs Seq("Hello")
      }
  }

  it should "append to an existing file" in { fs =>
    val path = defaultPath(fs)
    Files.writeString(path, "Hello\nHola\n")
    Source
      .single[(Path, String)]((path, "Goodbye"))
      .runWith(Logging.writeToFileSink)
      .map { _ =>
        Files
          .readAllLines(path)
          .asScala should contain theSameElementsInOrderAs Seq(
          "Hello",
          "Hola",
          "Goodbye"
        )
      }
  }
}
