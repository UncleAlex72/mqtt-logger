package mqttlogger

import akka.stream.alpakka.mqtt.MqttMessage
import akka.stream.scaladsl.{Sink, Source}
import akka.util.ByteString

import java.nio.file.Path
import java.time.{Clock, Instant, ZoneId}

class FilenameAndMessageFlowSpec
    extends JimFsSpec("filenameAndMessageFlowSpec") {

  val clock: Clock = Clock.fixed(
    Instant.ofEpochMilli(1637517386714L),
    ZoneId.of("Europe/London")
  )

  val mqttMessage: MqttMessage = MqttMessage("one/two", ByteString("Hello"))

  "The filename and message flow" should
    "use a filename based off the topic" in { fs =>
    Source
      .single(mqttMessage)
      .via(Logging.filenameAndMessageFlow(clock, fs))
      .map(_._1)
      .runWith(Sink.seq[Path])
      .map { paths =>
        paths should have size 1
        paths.head.getFileSystem should ===(fs)
        paths.head.toString should ===("/var/log/mqtt/one_two.log")
      }
  }

  it should "prepend the message with the current date and time" in { fs =>
    Source
      .single(mqttMessage)
      .via(Logging.filenameAndMessageFlow(clock, fs))
      .map(_._2)
      .runWith(Sink.seq[String])
      .map { paths =>
        paths should have size 1
        paths.head should ===("[2021-11-21T17:56:26.714Z] Hello")
      }

  }
}
