package mqttlogger

import akka.stream.scaladsl.{Sink, Source}
import reactivemongo.api.bson.{BSONDateTime, BSONDocument, BSONLong, BSONString}

import java.time.{Clock, Instant, ZoneId}
import scala.concurrent.Future

class RemoveTimestampFlowSpec extends AsyncSpec("RemoveTimestampFlowSpec") {

  val NOW: Long = 1631290266727L
  val THEN: Long = 1631290000000L

  "The RemoveTimestamp flow" should
    "add a timestamp of now if no timestamp key exists" in {
    flow(
      BSONDocument(Seq("key" -> BSONString("value")))
    ).map { bsonDocuments =>
      bsonDocuments should contain theSameElementsAs Seq(
        BSONDocument(
          Seq(
            "key" -> BSONString("value"),
            "timestamp" -> BSONDateTime(NOW)
          )
        )
      )
    }
  }

  it should "use the existing timestamp if it exists" in {
    flow(
      BSONDocument(
        Seq("key" -> BSONString("value"), "timestamp" -> BSONLong(THEN))
      )
    ).map { bsonDocuments =>
      bsonDocuments should contain theSameElementsAs Seq(
        BSONDocument(
          Seq(
            "key" -> BSONString("value"),
            "timestamp" -> BSONDateTime(THEN)
          )
        )
      )
    }
  }

  it should "overwrite the existing timestamp if it non-numeric" in {
    flow(
      BSONDocument(
        Seq("key" -> BSONString("value"), "timestamp" -> BSONString("now"))
      )
    ).map { bsonDocuments =>
      bsonDocuments should contain theSameElementsAs Seq(
        BSONDocument(
          Seq(
            "key" -> BSONString("value"),
            "timestamp" -> BSONDateTime(NOW)
          )
        )
      )
    }
  }

  def flow(bsonDocument: BSONDocument): Future[Seq[BSONDocument]] = {
    Source
      .single(bsonDocument)
      .via(
        Persistence.bsonTimestampFlow(
          Clock.fixed(Instant.ofEpochMilli(NOW), ZoneId.of("Europe/London"))
        )
      )
      .runWith(Sink.seq)
  }
}
