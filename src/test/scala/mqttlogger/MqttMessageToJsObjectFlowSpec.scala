package mqttlogger

import akka.stream.alpakka.mqtt.MqttMessage
import akka.stream.scaladsl.{Sink, Source}
import akka.util.ByteString
import play.api.libs.json.{JsObject, JsString}

import scala.concurrent.Future

class MqttMessageToJsObjectFlowSpec
    extends AsyncSpec("mqttMessageToJsObjectFlowSpec") {

  "The mqttMessageToJsObjectFlow" should
    "merge a topic and JSON object payload into a JSON object" in {
    flow("topical", """{"payload": "something"}""").map { jsObjects =>
      jsObjects should contain theSameElementsAs Seq(
        JsObject(
          Seq(
            "topic" -> JsString("topical"),
            "payload" -> JsString("something")
          )
        )
      )
    }
  }

  it should "treat non-json object payloads as a string" in {
    flow("topical", """"string"""").map { jsObjects =>
      jsObjects should contain theSameElementsAs Seq(
        JsObject(
          Seq(
            "topic" -> JsString("topical"),
            "payload" -> JsString(""""string"""")
          )
        )
      )
    }
  }

  it should "treat non-parseable payloads as a string" in {
    flow("topical", """string""").map { jsObjects =>
      jsObjects should contain theSameElementsAs Seq(
        JsObject(
          Seq(
            "topic" -> JsString("topical"),
            "payload" -> JsString("string")
          )
        )
      )
    }
  }

  def flow(topic: String, payload: String): Future[Seq[JsObject]] = {
    val message: MqttMessage =
      MqttMessage(topic = topic, payload = ByteString(payload))
    Source
      .single(message)
      .via(Persistence.mqttMessageToJsObjectFlow())
      .runWith(Sink.seq)
  }
}
