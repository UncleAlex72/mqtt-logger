package mqttlogger

import akka.stream.scaladsl.{Sink, Source}
import play.api.libs.json.{JsObject, JsString}
import reactivemongo.api.bson.{BSONDocument, BSONString}

import scala.concurrent.Future

class RemoveBsonIdSpec extends AsyncSpec("RemoveBsonIdSpec") {

  "The RemoveBsonId flow" should
    "remove _id from any objects" in {
    flow(
      BSONDocument(Seq("_id" -> BSONString("id"), "key" -> BSONString("value")))
    ).map { bsonDocuments =>
      bsonDocuments should contain theSameElementsAs Seq(
        BSONDocument(
          Seq(
            "key" -> BSONString("value")
          )
        )
      )
    }
  }

  def flow(bsonDocument: BSONDocument): Future[Seq[BSONDocument]] = {
    Source
      .single(bsonDocument)
      .via(Persistence.removeBsonIdFlow())
      .runWith(Sink.seq)
  }
}
