package mqttlogger

import akka.actor.ActorSystem
import akka.testkit.TestKit
import de.flapdoodle.embed.mongo.MongodStarter
import de.flapdoodle.embed.mongo.config.Net
import org.scalatest.flatspec.FixtureAsyncFlatSpecLike
import org.scalatest.matchers.should.Matchers
import org.scalatest.{BeforeAndAfterAll, FutureOutcome}
import reactivemongo.api.AsyncDriver

abstract class MongoDbSpec(name: String)
    extends TestKit(ActorSystem(name))
    with FixtureAsyncFlatSpecLike
    with Matchers
    with BeforeAndAfterAll {

  val driver: AsyncDriver = AsyncDriver()

  type FixtureParam = String

  override def withFixture(test: OneArgAsyncTest): FutureOutcome = {
    val embeddedMongoDb = MongoDbSpec.start()

    val param: FixtureParam =
      s"mongodb://localhost:${embeddedMongoDb.port}/mqtt-logger"

    withFixture(test.toNoArgAsyncTest(param)).onCompletedThen { _ =>
      embeddedMongoDb.shutdown()
    }
  }

  final override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

}

object MongoDbSpec {

  private val starter: MongodStarter = MongodStarter.getDefaultInstance

  trait EmbeddedMongoDb {
    val port: Int
    def shutdown(): Unit
  }

  import de.flapdoodle.embed.mongo.MongodExecutable
  import de.flapdoodle.embed.mongo.config.MongodConfig
  import de.flapdoodle.embed.mongo.distribution.Version
  import de.flapdoodle.embed.process.runtime.Network

  def start(): EmbeddedMongoDb =
    new EmbeddedMongoDb {
      override val port: Int = Network.getFreeServerPort
      val mongodConfig: MongodConfig =
        MongodConfig.builder
          .version(Version.V4_0_12)
          .net(new Net(port, Network.localhostIsIPv6))
          .build
      val mongodExecutable: MongodExecutable = starter.prepare(mongodConfig)
      mongodExecutable.start

      override def shutdown(): Unit = {
        mongodExecutable.stop()
      }
    }
}
