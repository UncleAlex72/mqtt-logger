package mqttlogger

import akka.stream.scaladsl.{Sink, Source}
import play.api.libs.json.{JsObject, JsString}
import reactivemongo.api.bson.{BSONDocument, BSONString}

import scala.concurrent.Future

class JsonToBsonFlowSpec extends AsyncSpec("JsonToBsonFlowSpec") {

  "The jsonToBsonFlow" should
    "convert json into bson" in {
    flow(JsObject(Seq("key" -> JsString("value")))).map { bsonDocuments =>
      bsonDocuments should contain theSameElementsAs Seq(
        BSONDocument(
          Seq(
            "key" -> BSONString("value")
          )
        )
      )
    }
  }

  def flow(jsObject: JsObject): Future[Seq[BSONDocument]] = {
    Source
      .single(jsObject)
      .via(Persistence.jsonToBsonFlow())
      .runWith(Sink.seq)
  }
}
