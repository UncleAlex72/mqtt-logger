package mqttlogger

import akka.actor.ActorSystem
import akka.testkit.TestKit
import com.google.common.jimfs.{Configuration, Jimfs}
import org.scalatest.flatspec.FixtureAsyncFlatSpecLike
import org.scalatest.matchers.should.Matchers
import org.scalatest.{BeforeAndAfterAll, FutureOutcome}
import reactivemongo.api.AsyncDriver

import java.nio.file.{FileSystem, Files}

abstract class JimFsSpec(name: String)
    extends TestKit(ActorSystem(name))
    with FixtureAsyncFlatSpecLike
    with Matchers
    with BeforeAndAfterAll {

  val driver: AsyncDriver = AsyncDriver()

  type FixtureParam = FileSystem

  override def withFixture(test: OneArgAsyncTest): FutureOutcome = {
    val fs = Jimfs.newFileSystem(Configuration.unix())
    Files.createDirectories(fs.getPath("/", "var", "log", "mqtt"))
    withFixture(test.toNoArgAsyncTest(fs))
  }

  final override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

}
