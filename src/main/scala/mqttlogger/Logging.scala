package mqttlogger

import akka.stream.alpakka.mqtt.MqttMessage
import akka.stream.scaladsl.{Flow, Keep, Sink}
import akka.{Done, NotUsed}
import com.typesafe.scalalogging.StrictLogging

import java.nio.file.{FileSystem, Files, Path}
import java.nio.file.StandardOpenOption._
import java.time.Clock
import scala.concurrent.{ExecutionContext, Future}

object Logging extends StrictLogging {

  def sink(clock: Clock, fs: FileSystem)(implicit
      ec: ExecutionContext
  ): Sink[MqttMessage, Future[Done]] = {
    filenameAndMessageFlow(clock, fs).toMat(writeToFileSink)(Keep.right)
  }

  def filenameAndMessageFlow(
      clock: Clock,
      fs: FileSystem
  ): Flow[MqttMessage, (Path, String), NotUsed] = {
    val loggingDirectory = fs.getPath("/", "var", "log", "mqtt")
    Flow[MqttMessage].map { mqttMessage =>
      val topic = mqttMessage.topic.replace('/', '_')
      val payload = mqttMessage.payload.utf8String
      val logFile = loggingDirectory.resolve(s"$topic.log")
      val now = clock.instant().atZone(clock.getZone).toOffsetDateTime
      val message = s"[$now] $payload"
      (logFile, message)
    }
  }

  def writeToFileSink: Sink[(Path, String), Future[Done]] = {
    Sink.foreach[(Path, String)] {
      case (logFile, message) =>
        logger.info(s"Writing [$message] to [$logFile]")
        try {
          Files.writeString(
            logFile,
            s"$message\n",
            WRITE,
            CREATE,
            APPEND
          )
        } catch {
          case ex: Exception =>
            logger.error(s"Cannot write to file [$logFile]", ex)
        }
    }
  }

}
