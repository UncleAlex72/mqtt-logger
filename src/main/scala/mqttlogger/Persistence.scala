package mqttlogger

import akka.NotUsed
import akka.stream.alpakka.mqtt.MqttMessage
import akka.stream.scaladsl.{Flow, Keep, Sink, Source}
import com.typesafe.scalalogging.StrictLogging
import play.api.libs.json.{JsObject, JsString, Json}
import reactivemongo.api.bson.collection.{BSONCollection, BSONSerializationPack}
import reactivemongo.api.bson.{BSONDateTime, BSONDocument, BSONNumberLike}
import reactivemongo.api.indexes.Index.Aux
import reactivemongo.api.indexes.{Index, IndexType}
import reactivemongo.api.{AsyncDriver, FailoverStrategy, MongoConnection}
import reactivemongo.play.json.compat._

import java.time.Clock
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Success, Try}

object Persistence extends StrictLogging {

  val TIMESTAMP = "timestamp"
  val TOPIC = "topic"
  val PAYLOAD = "payload"
  val _ID = "_id"
  val MESSAGES = "messages"

  def sink(
      clock: Clock,
      asyncDriver: AsyncDriver,
      maybeExpireAfter: Option[FiniteDuration],
      uri: String
  )(implicit
      ec: ExecutionContext
  ): Sink[MqttMessage, Future[BSONCollection]] = {
    mqttMessageToJsObjectFlow()
      .via(jsonToBsonFlow())
      .via(removeBsonIdFlow())
      .via(bsonTimestampFlow(clock))
      .toMat(mongoDbSink(asyncDriver, maybeExpireAfter, uri))(Keep.right)
  }

  def mqttMessageToJsObjectFlow(): Flow[MqttMessage, JsObject, NotUsed] = {
    Flow[MqttMessage].flatMapConcat { message =>
      val topic = message.topic
      val payload = message.payload.utf8String
      logger.info(s"Received payload [$payload] in topic [$topic]")
      val jsObj: JsObject = Try(Json.parse(payload)) match {
        case Success(jsObject: JsObject) =>
          jsObject
        case _ =>
          JsObject(Seq(PAYLOAD -> JsString(payload)))
      }
      Source.single(jsObj + (TOPIC -> JsString(topic)))
    }
  }

  def jsonToBsonFlow(): Flow[JsObject, BSONDocument, NotUsed] = {
    Flow[JsObject].map { jsObject =>
      val bsonDocument: BSONDocument = jsObject
      bsonDocument
    }
  }

  def removeBsonIdFlow(): Flow[BSONDocument, BSONDocument, NotUsed] = {
    Flow[BSONDocument].map(_ -- _ID)
  }

  def bsonTimestampFlow(
      clock: Clock
  ): Flow[BSONDocument, BSONDocument, NotUsed] = {
    Flow[BSONDocument].map { bson =>
      val maybeInstant: Option[Long] = for {
        timestampValue <- bson.get(TIMESTAMP)
        timestampNumber <- timestampValue.asOpt[BSONNumberLike]
        timestamp <- timestampNumber.toLong.toOption
      } yield {
        timestamp
      }
      val currentInstant = maybeInstant.getOrElse(clock.millis())
      bson ++ (TIMESTAMP -> BSONDateTime(currentInstant))
    }
  }

  def mongoDbSink(
      asyncDriver: AsyncDriver,
      maybeExpireAfter: Option[FiniteDuration],
      uri: String
  )(implicit
      ec: ExecutionContext
  ): Sink[BSONDocument, Future[BSONCollection]] = {
    val eventualCollection = for {
      parsedUri <- MongoConnection.fromString(uri)
      dbName <- Future.fromTry(
        parsedUri.db
          .toRight(
            new NoSuchElementException(s"URI $uri does not contain a database")
          )
          .toTry
      )
      connection <- asyncDriver.connect(uri)
      db <- connection.database(dbName)
      indexManager = db.indexesManager.onCollection(MESSAGES)
      _ <- indexManager.ensure(createIndex(TIMESTAMP, maybeExpireAfter))
      _ <- indexManager.ensure(createIndex(TOPIC, None))
    } yield {
      db.collection[BSONCollection](
        name = MESSAGES,
        failoverStrategy = FailoverStrategy.default
      )
    }
    eventualCollection.foreach { collection =>
      logger.info(
        s"Successfully created collection [${collection.name}] at [$uri]"
      )
    }
    Sink
      .foreachAsync[BSONDocument](8) { bson =>
        val eventualWriteResult = for {
          collection <- eventualCollection
          writeResult <- collection.insert(ordered = false).one(bson)
        } yield {
          writeResult
        }
        eventualWriteResult.map { writeResult =>
          if (!writeResult.ok) {
            val errorMessages = writeResult.writeErrors.map { writeError =>
              s"${writeError.code}: ${writeError.errmsg}"
            }
            errorMessages.foreach(msg => logger.error(msg))
          }
        }
      }
      .mapMaterializedValue(_.flatMap(_ => eventualCollection))
  }

  def createIndex(
      columnName: String,
      maybeExpiryDuration: Option[FiniteDuration]
  ): Aux[BSONSerializationPack.type] = {
    Index(BSONSerializationPack)(
      key = Seq(columnName -> IndexType.Ascending),
      name = Some(s"${columnName}Index"),
      unique = false,
      background = false,
      dropDups = false,
      sparse = false,
      expireAfterSeconds = maybeExpiryDuration.map(_.toSeconds.toInt),
      storageEngine = None,
      weights = None,
      defaultLanguage = None,
      languageOverride = None,
      textIndexVersion = None,
      sphereIndexVersion = None,
      bits = None,
      min = None,
      max = None,
      bucketSize = None,
      collation = None,
      wildcardProjection = None,
      version = None,
      partialFilter = None,
      options = BSONDocument.empty
    )
  }
}
