package mqttlogger.config

import pureconfig._
import pureconfig.generic.semiauto._

import scala.concurrent.duration.FiniteDuration

case class MqttConfiguration(url: String, clientId: String, topics: Seq[String])
case class MongoDbConfiguration(url: String, expireAfter: FiniteDuration)

case class MqttLoggerConfiguration(
    mqtt: MqttConfiguration,
    mongoDb: MongoDbConfiguration
)

object MqttLoggerConfiguration {

  implicit val mqttConfigurationReader: ConfigReader[MqttConfiguration] =
    deriveReader[MqttConfiguration]
  implicit val mongoDbConfigurationReader: ConfigReader[MongoDbConfiguration] =
    deriveReader[MongoDbConfiguration]
  implicit val mqttLoggerConfigurationReader
      : ConfigReader[MqttLoggerConfiguration] =
    deriveReader[MqttLoggerConfiguration]

}
