package mqttlogger

import akka.actor.ActorSystem
import akka.stream.alpakka.mqtt.scaladsl.MqttSource
import akka.stream.alpakka.mqtt.{
  MqttConnectionSettings,
  MqttMessage,
  MqttQoS,
  MqttSubscriptions
}
import akka.stream.scaladsl.{Broadcast, Sink, Source}
import akka.{Done, NotUsed}
import com.typesafe.scalalogging.StrictLogging
import mqttlogger.config.MqttLoggerConfiguration
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence
import pureconfig.ConfigSource
import reactivemongo.api.AsyncDriver
import reactivemongo.api.bson.collection.BSONCollection

import java.nio.file.FileSystems
import java.time.Clock
import scala.concurrent.{ExecutionContext, Future}

object Application extends App with StrictLogging {

  val configurationFile = Option(System.getenv("MQTT_LOGGER_CONF_FILE"))
    .getOrElse("/etc/mqtt-logger/mqtt-logger.conf")

  val mqttLoggerConfiguration =
    ConfigSource
      .file(configurationFile)
      .load[MqttLoggerConfiguration] match {
      case Right(mqttSqueezeboxConfiguration: MqttLoggerConfiguration) =>
        mqttSqueezeboxConfiguration
      case Left(configReaderFailures) =>
        logger.error(
          s"Configuration parsing failed:\n${configReaderFailures.prettyPrint(2)}"
        )
        throw new IllegalStateException("Configuration parsing failed.")
    }

  implicit val actorSystem: ActorSystem = ActorSystem()
  implicit val ec: ExecutionContext = actorSystem.dispatcher

  val mqttSource: Source[MqttMessage, Future[Done]] = {
    val mqttConfiguration = mqttLoggerConfiguration.mqtt
    MqttSource.atMostOnce(
      MqttConnectionSettings(
        mqttConfiguration.url,
        mqttConfiguration.clientId,
        new MemoryPersistence
      ).withAutomaticReconnect(true),
      MqttSubscriptions(
        mqttConfiguration.topics.map(_ -> MqttQoS.AtLeastOnce).toMap
      ),
      bufferSize = 8
    )
  }

  val driver = AsyncDriver()

  private val clock = Clock.systemDefaultZone()

  val persistingSink: Sink[MqttMessage, Future[BSONCollection]] =
    Persistence.sink(
      clock,
      driver,
      Some(mqttLoggerConfiguration.mongoDb.expireAfter),
      mqttLoggerConfiguration.mongoDb.url
    )

  val loggingSink: Sink[MqttMessage, Future[Done]] = {
    Logging.sink(clock, FileSystems.getDefault)
  }

  val sink: Sink[MqttMessage, NotUsed] =
    Sink.combine(persistingSink, loggingSink)(Broadcast[MqttMessage](_))

  mqttSource.runWith(sink)
}
